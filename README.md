###
最终ts+webpack的模板文件，新增了路由


// Vue2语法

 export default defineComponent ({
 name:'Hello',
 data:()=>{
     return{
         Str : 'Hello World',
     }
 },
 template:` 
     <div id="First">{{Str}}</div>
 `
 })

// Vue3语法  (推荐使用)

export default defineComponent ({
    name:'Hello',
    setup(){
        const state = reactive({
            name: 'tom'
        })

        return{
            state
        }
    },
    template:` 
        <div id="First">{{state.name}}</div>
    `
})



export class Test {
    public static Str:string = "这是我的命名空间";
    public constructor() {
    }
    public static Fun():void {
        console.log(this.Str);
    }
}

对于上述的静态成员变量和函数，只可在内部进行访问，外部申明无法直接访问
静态方法调用直接在类上进行，不能在类的实例上调用。静态方法通常用于创建实用程序函数。
45行这句话的意思就是，若调用里面的Fun（）方法，可以直接通过Test.Fun()进行调用，无需进行实例化（比如let Exa = new Test();Exa.Fun()）
注意：若上述的Fun函数前面的public改为private,则外部无法访问该函数，只能在该类内部进行访问

构造器的私有和公共决定了能否可以直接访问类中的方法而不需要实例化,而类中的属性的私有和公有决定了外部能否调用


图片文件的引入：
需要使用require（）语法，例如：test:require('../../assest/11.jpg')


type Props = {
    isShowDrawer :Boolean
}
const deliveryModelClass = defineProps<Props>()

这种子组件接收的props参数，无法使用(只能在vue脚手架下的ts环境进行使用)

const emits = defineEmits(["reSetDrawerInitValue"])
上述中，也是无法在这种webpack+ts构建的vue环境中使用子组件调用父组件方法



对于这种环境创建的项目，可以使用如下两种方式进行子组件调用父组件的方法

方式一：(该方式是将父组件的方法以props形式传递给子组件，这样子组件可以内部直接使用父组件方法)

father.ts
<!-- html -->
<Child :c_func=f_func></Child>
<!-- javascript -->
setup(props,ctx){
    const f_func = () =>{
        console.log("这里是父组件的方法")"
    }
    return{
        f_func
    }
}

child.ts

<!-- javascript -->
props{
    c_func:{
        type:Function,,
        default:null
    }
}
setup(props,ctx){
    const OtherFunc = () =>{
        props.c_func()   //------>调用到了父组件的方法
    }
    return{
        OtherFunc
    }
}

方式二：(这种方式是将父方法定义绑定给子组件的上下文中，这样子组件直接使用ctx调用父组件的方法)

father.ts
<Child @c_func=f_func></Child>
<!-- javascript -->
setup(props,ctx){
    const f_func = () =>{
        console.log("这里是父组件的方法")"
    }
    return{
        f_func
    }
}

child.ts
<!-- javascript -->
setup(props,ctx){
    const OtherFunc = () =>{
        ctx.emit('c_func')
    }
    return{
        OtherFunc
    }
}



在使用fetch请求时，可以使用Response对象的body属性来获取响应的数据流。然后，可以使用ReadableStream对象来读取流中的数据。具体步骤如下：

使用fetch方法发送请求，获取Response对象。

fetch(url)
  .then(response => {
    // 获取响应的数据流
    const stream = response.body;
    // 使用ReadableStream对象读取流中的数据
    const reader = stream.getReader();
    // 逐句读取数据
    function read() {
      reader.read().then(({ done, value }) => {
        if (done) {
          // 数据读取完成
          return;
        }
        // 将数据显示在页面上
        const text = new TextDecoder().decode(value);
        document.body.innerHTML += text;
        // 继续读取下一句数据
        read();
      });
    }
    // 开始读取数据
    read();
  });


获取响应的数据流，并使用ReadableStream对象读取流中的数据。

const stream = response.body;
const reader = stream.getReader();


使用递归函数read()逐句读取数据，并将数据显示在页面上。

function read() {
  reader.read().then(({ done, value }) => {
    if (done) {
      return;
    }
    const text = new TextDecoder().decode(value);
    document.body.innerHTML += text;
    read();
  });
}


开始读取数据。

read();

需要注意的是，由于数据是逐句读取的，因此在页面上显示时可能会出现断句的情况。如果需要保持完整的句子，可以使用缓存数组来存储读取的数据，直到读取到完整的句子再显示在页面上。