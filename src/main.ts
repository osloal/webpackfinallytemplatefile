import { createApp , onMounted} from 'vue'
import 'element-plus/dist/index.css'
import './utils/Style/Global.less'
import router from './router/index'

let app = createApp({})
app.use(router)
app.mount('#app')