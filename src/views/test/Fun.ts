import { defineComponent } from "@vue/runtime-core";
 import Global from "../../utils/Global/Global";
 import { ref ,reactive } from "vue";
 import { newRegister,fileSave ,getFile } from "../../utils/XHR/Controller/users";
 import { ElMessage } from "element-plus";
 export default defineComponent ({
    name:'Second',
    setup(){
        const Str=ref('这是我的第二个子节点')
        interface tJsondata2 {
            [key:string]:string
        }
        const tJsondata2:tJsondata2 = {}
        const UserInfo = reactive({
            username:'oslo',
            age:25,
            desc:'练习时长两年半的前端练习生'
        })
        const fsUserInfo = reactive({
            foldername : 'oslo',
            filename:'personConfig',
            data:UserInfo
        })
        const filePath = reactive({
            foldername : 'oslo',
            filename:'personConfig',
        })
        tJsondata2.name = '这是我的第二个子节点'
        const simulationRegister = async () => {
            let data = await newRegister('newRegister',UserInfo)
            console.log(data)
            Tips(data)
        }
        const newFileSave = async () =>{
            let data = await fileSave('userInfoFile',fsUserInfo)
            console.log(data)
            Tips(data)
        }
        const getUserInfoFile = async () => {
            let data = await getFile('getUserInfo',filePath)
            console.log(JSON.parse(data.data))
            Tips(data)
        }
        interface data{
            [key:string]:any
        }
        const Tips = (data:data) =>{
            if(data.code === 0){
                ElMessage.success(data.msg)
            }else{
                ElMessage.error(data.msg || '更新失败')
            }
        }
        const Jsondata = reactive({
        })
        console.log(Object.keys(Jsondata).length)
        const myObj = reactive({ name: 'John', age: 30 }) ;
        let JS = Object.assign({}, myObj);
        

        console.log(JS)

        return{
            Str,
            Global,
            simulationRegister,
            newFileSave,
            getUserInfoFile,
            Tips,
            img:require('../../assest/333.jpg')
        }
    },
    
    template:` 
        <div id="Second" v-if="Global.Status.isLogin?true:false">{{Str}}{{Global.Status.isLogin}}</div>
        <div @click="simulationRegister">发送创建文件夹请求</div>
        <div @click="newFileSave">发送写文件请求</div>
        <div @click="getUserInfoFile">请求文件内容详情</div>
        <img :src=img al="爱困" title="aiku">
    `
 })