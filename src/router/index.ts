import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Home",
        component: () => import("../views/plugins/Main/index"),
        meta: {
            index: 1
        }
    },
    {
        path: "/temp",
        name: "Temp",
        component: () => import("../views/plugins/temporary/index"),
        meta: {
            index: 1
        }
    }
];
let router = createRouter({
    history: createWebHashHistory(),
    routes
});
export default router;
